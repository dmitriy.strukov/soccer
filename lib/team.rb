class Team < Dry::Struct
  attribute :name, Types::Strict::String

  attr_reader :name, :players

  def initialize(name:)
    @name    = name
    @players = []
  end

  def add_player(player)
    player.team = self
    players.push(player)
  end

  def inspect
    "Team(object_id: #{object_id}, name: #{name}, players: #{players.map(&:full_name).join(',')})"
  end
end
