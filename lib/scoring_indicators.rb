module ScoringIndicators
  def top_players_in_team(team:, indicator_name:, qnt: 5)
    indicators = team.players.map(&:indicators).flatten

    indicators.sort_by do |indicator|
      -indicator.instance_variable_get(:"@#{indicator_name}")
    end.first(qnt)
  end

  module_function :top_players_in_team

  def top_players_in_teams(teams:, indicator_name:, qnt: 5)
    result = teams.map do |team|
      top_players_in_team(team: team, indicator_name: indicator_name)
    end.flatten

    result.sort_by do |indicator|
      -indicator.instance_variable_get(:"@#{indicator_name}")
    end.first(qnt)
  end

  module_function :top_players_in_teams

  def indicator_done?(player:, value:, indicator_name:)
    player.indicators.any? { |indicator| indicator.instance_variable_get(:"@#{indicator_name}") >= value }
  end

  module_function :indicator_done?
end
