class PlayerIndicators
  DEFAULT_INDICATORS = {
    distance: 0,
    success_transfers: 0,
    failure_transfers: 0,
    goals: 0
  }.freeze

  attr_reader :match, :player

  def initialize(match, player)
    @match = match
    @player = player

    after_initialize_callbacks!
  end

  def add_multiple_indicators(indicators)
    indicators.each do |indicator_name, indicator_value|
      instance_variable_set(:"@#{indicator_name}", indicator_value)
    end
  end

  def inspect
    "PlayerIndicators(object_id: #{object_id}, player: #{player.full_name}, team: #{player.team.name}, distance: #{distance}, success_transfers: #{success_transfers}, failure_transfers: #{failure_transfers}, goals: #{goals})"
  end

  private

    def after_initialize_callbacks!
      @match.add_indicator(self)
      @player.add_indicator(self)
    end

    DEFAULT_INDICATORS.each do |indicator_name, indicator_value|
      define_method(indicator_name) do
        instance_variable_get(:"@#{indicator_name}") || indicator_value
      end
    end
end
