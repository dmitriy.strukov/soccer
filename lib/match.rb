class Match
  attr_reader :owner_team, :guest_team
  attr_reader :indicators

  def initialize(owner_team, guest_team)
    @owner_team = owner_team
    @guest_team = guest_team

    @indicators = []
  end

  def add_indicator(indicator)
    @indicators.push(indicator)
  end

  def inspect
    "Match(object_id: #{object_id}, owner_team: #{owner_team.name}, guest_team: #{guest_team.name})"
  end
end
