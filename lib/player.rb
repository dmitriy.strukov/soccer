class Player < Dry::Struct
  attribute :first_name, Types::Strict::String
  attribute :last_name,  Types::Strict::String

  attribute :age, Types::Integer

  attr_reader :first_name, :last_name, :age
  attr_reader :team, :indicators

  def initialize(first_name:, last_name:, age:)
    @first_name = first_name
    @last_name  = last_name
    @age = age

    @indicators = []
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def team_name
    team.name
  end

  def add_indicator(indicator)
    @indicators.push(indicator)
  end

  def team=(value)
    raise SyntaxError, "#{full_name} already has a team, use transfer method!" unless team.nil?

    @team = value
  end

  def inspect
    "Player(object_id: #{object_id}, age: #{age}, full_name: #{full_name}, team_name: #{team_name})"
  end
end
