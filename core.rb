require 'dry-types'
require 'dry-struct'

module Types
  include Dry::Types(default: :nominal)
end

require './lib/player.rb'
require './lib/player_indicators.rb'
require './lib/team.rb'
require './lib/match.rb'

require './lib/scoring_indicators.rb'

require 'faker'
require 'pry'

owner_team = Team.new(name: Faker::Name.name)
guest_team = Team.new(name: Faker::Name.name)

10.times do
  player = Player.new(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, age: rand(18..40))
  owner_team.add_player(player)
end

10.times do
  player = Player.new(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, age: rand(18..40))
  guest_team.add_player(player)
end

100.times do
  match = Match.new(owner_team, guest_team)

  owner_team.players.each do |player|
    player_indicators = PlayerIndicators.new(match, player)
    player_indicators.add_multiple_indicators(distance: rand(5000..11_000), success_transfers: rand(50..200), failure_transfers: rand(10..50), goals: rand(0..5))
  end

  guest_team.players.each do |player|
    player_indicators = PlayerIndicators.new(match, player)
    player_indicators.add_multiple_indicators(distance: rand(5000..11_000), success_transfers: rand(50..200), failure_transfers: rand(10..50), goals: rand(0..5))
  end
end
